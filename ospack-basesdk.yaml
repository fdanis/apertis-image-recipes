{{- $architecture := or .architecture "amd64" }}
{{- $type := or .type "basesdk" -}}
{{- $mirror := or .mirror "https://repositories.apertis.org/apertis/" -}}
{{- $suite := or .suite "v2023dev2" -}}
{{- $timestamp := or .timestamp "" -}}
{{- $snapshot := or .snapshot "" -}}
{{- $ospack := or .ospack (printf "ospack_%s-%s-%s" $suite $architecture $type) -}}
{{- $pack := or .pack "true" -}}
{{- $stable := or .stable "" -}}
{{- $osname := or .osname "apertis" -}}
{{- $keyring := or .keyring (printf "%s-archive-keyring" $osname) -}}

architecture: {{ $architecture }}

actions:
  - action: debootstrap
    suite: {{if eq $snapshot ""}} {{ $suite }} {{else}} {{ $suite }}/snapshots/{{ $snapshot }} {{end}}
    components:
      - target
    mirror: {{ $mirror }}
    variant: minbase
    keyring-package: {{ $keyring }}
    keyring-file: keyring/{{ $keyring }}.gpg
    merged-usr: false

  - action: overlay
    source: overlays/locale-default-c-utf8

  - action: overlay
    source: overlays/locale-gen

  - action: overlay
    description: Work around "Hash Sum Mismatch" errors, https://phabricator.collabora.com/T15071
    source: overlays/apt-disable-http-pipelining

  # Add image version information
  - action: run
    description: "Setting up image version metadata"
    chroot: true
    script: scripts/setup_image_version.sh {{ $osname }} {{ $suite }} '{{ $timestamp }}' collabora {{ $type }}

  - action: run
    description: "Add extra apt sources"
    chroot: true
    script: scripts/apt_source.sh -m {{ $mirror }} -r {{ $suite }} --sources {{if eq $stable "true"}} --updates --security {{end}} target development sdk {{if ne $snapshot ""}} --snapshot {{ $snapshot }} {{end}}

  - action: run
    description: "Switch to the latest coreutils package (GPLv3)"
    chroot: true
    script: scripts/replace-gplv2-packages-for-dev-env.sh

  - action: apt
    description: "Switch to gpgv package (GPLv3)"
    packages:
      - gpgv

  - action: apt
    description: "Core packages"
    packages:
      - sudo
      - apt-transport-https
      - ca-certificates
      - initramfs-tools

  - action: apt
    description: "Base packages"
    packages:
      - busybox
      - dbus-user-session

  - action: apt
    description: "Networking packages"
    packages:
      - connman
      - wpasupplicant

  - action: apt
    description: "AppArmor packages"
    packages:
      - apparmor
      - chaiwala-apparmor-profiles

  - action: apt
    description: "Test environment packages"
    packages:
      - net-tools
      - openssh-client
      - openssh-server
      - vim.tiny

  - action: apt
    description: "Target packages"
    packages:
      - adduser
      - alsa-ucm-conf
      - apparmor
      - apparmor-profiles
      - apt
      - apt-transport-https
      - apt-utils
      - avahi-daemon
      - bluez
      - bluez-obexd
      - bzip2
      - ca-certificates
      - connman
      - debconf-i18n
      - file
      - fonts-dejavu-extra
      - gnupg
      - gstreamer1.0-pipewire
      - gstreamer1.0-plugins-good
      - gstreamer1.0-pulseaudio
      - gvfs
      - initramfs-tools
      - iproute2
      - iptables
      - kmod
      - libglib2.0-bin
      - liblockfile-bin
      - libnss-myhostname
      #- libproxy1-pacrunner
      - libwebkit2gtk-4.0-37
      - libspa-0.2-bluetooth
      - locales
      - lsb-base
      - lzma
      - mawk
      - mobile-broadband-provider-info
      - net-tools
      - netbase
      - ofono
      - openssh-client
      - openssh-server
      #- pacrunner
      - pipewire
      - pipewire-pulse
      - plymouth
      - plymouth-themes
      - policykit-1
      - pulseaudio-utils
      - sudo
      - systemd-sysv
      - udev
      - usb-modeswitch
      - vim-tiny
      - whiptail
      - wireplumber
      - wpasupplicant
      - xauth
      - xdg-user-dirs
      - xwayland

  - action: apt
    description: "Development packages"
    packages:
      - apparmor-utils
      - autoconf-archive
      - automake
      - autopoint
      - autotools-dev
      - bash
      - binfmt-support # for devroots and building foreign debos images through qemu-user-static
      - bison
      - build-essential
      - bustle-pcap
      - chrpath
      - cmake
      - cmake-data
      #- connman-tests
      - d-feet
      - debhelper
      - debootstrap
      - debos
      - device-tree-compiler
      - dosfstools
      - fakeroot
      - flex
      - gawk
      - gcc
      - gdb
      - gdbserver
      - gir1.2-secret-1
      - git
      - git-buildpackage
      - gperf
      - gstreamer1.0-tools
      - gtk-doc-tools
      - iputils-ping
      - less
      - libasound2-dev
      - libbluetooth-dev
      - libcurl4-nss-dev
      - libffi-dev
      - libgirepository1.0-dev
      - libglib2.0-0-dbgsym
      - libglib2.0-dev
      - libgstreamer-plugins-base1.0-dev
      - libgstreamer1.0-dev
      - libgtk2.0-dev
      - libgtk-3-0-dbgsym
      - libgudev-1.0-dev
      - libhyphen-dev
      - libicu-dev
      - libjpeg-dev
      - libnotify-dev
      - libpango1.0-dev
      - libpng-dev
      - libpoppler-glib-dev
      - libproxy-dev
      - libpulse-dev
      - libsecret-1-0
      - libsecret-1-dev
      - libsecret-common
      - libsoup2.4-dev
      - libsqlite3-dev
      - libtool
      - libxslt1-dev
      - libxt-dev
  {{ if eq $architecture "armhf" }}
      - linux-headers-armmp
  {{ else }}
      - linux-headers-{{$architecture}}
  {{ end }}
      - lsb-release
      - ltrace
      - openssh-client
      - openssh-server
      - ostree
      - parted
      - pavucontrol
      - pkg-config
      - python3-jinja2
      - rsync
      - ruby
      - slimit
      - strace
      - symlinks
      - valgrind
      - wget
      - xinput

  - action: apt
    description: "SDK packages"
    packages:
      - adduser
      #- apertis-customizations
      - apertis-dev-tools
      #- apertis-docs
      - apparmor
      - apparmor-profiles
      - apparmor-utils
      - apt
      - apt-transport-https
      - apt-utils
      - automake
      - autopoint
      - autotools-dev
      - avahi-daemon
      - bash
      - bison
      - blueman
      - bluez
      - bluez-obexd
      - bmap-tools
      - build-essential
      - bustle
      - bustle-pcap
      - bzip2
      - ca-certificates
      - ccache
      #- chaiwala-sdk-default-settings
      - chrpath
      - clang
      - cmake
      - cntlm
      - connman
      #- connman-tests
      - d-feet
      - dbus-user-session
      - dbus-x11
      - debconf-i18n
      - debhelper
      - devhelp
      - devscripts
      - dh-autoreconf
      - dh-exec
      - docker.io
      - dosfstools
      # needed for flatpak-builder
      - elfutils
      - evince
      - fakeroot
      - file
      - firefox-esr
      - flatpak-builder
      - flex
      # needed for flatpak-builder
      - fuse
      - g++-aarch64-linux-gnu
      - g++-arm-linux-gnueabihf
      - gawk
      - gcc
      - gcc-aarch64-linux-gnu
      - gcc-arm-linux-gnueabihf
      - gdb
      #- gdb-doc
      - gdb-multiarch
      - gdbserver
      - gettext-doc
      - gir1.2-secret-1
      - git
      - glibc-doc
      - gnome-desktop-testing
      - gnome-devel-docs
      - gnome-icon-theme
      - gnupg
      - gperf
      - gstreamer1.0-pipewire
      - gstreamer1.0-plugins-good
      - gstreamer1.0-pulseaudio
      - gstreamer1.0-tools
      - gtk-doc-tools
      - ibus-doc
      - imx-code-signing-tool
      - initramfs-tools
      - iproute2
      - iptables
      - iputils-ping
      - kmod
      - lcov
      - less
      - libasound2-dev
      - libbluetooth-dev
      - libcairo2-doc
      - libcurl4-nss-dev
      - libelf-dev
      - libffi-dev
      - libgdata-doc
      - libgirepository1.0-dev
      - libglib2.0-dev
      - libgstreamer-plugins-base1.0-dev
      - libgstreamer1.0-dev
      - libicu-dev
      - libjpeg-dev
      - libjson-glib-doc
      - liblockfile-bin
      - libnss-myhostname
      - libpango1.0-dev
      - libpng-dev
      - libpoppler-glib-dev
      - libproxy-dev
      # - libproxy1-pacrunner
      - libpulse-dev
      - libsecret-1-0
      - libsecret-1-dev
      - libsecret-common
      - libsoup2.4-dev
      - libsoup2.4-doc
      - libspa-0.2-bluetooth
      - libsqlite3-dev
      - libtool
      - libwebp-dev
      - libxml2-doc
      - libxml2-utils
      - libxslt1-dev
      - libxt-dev
      - lightdm
      #- linux-firmware
      - linux-perf
      - lsb-base
      - lsb-release
      - ltrace
      - lzma
      - mawk
      - mousepad
      - murrine-themes
      - net-tools
      - netbase
      - openssh-client
      - openssh-server
      - osc
      # - pacrunner
      - pavucontrol
      - pipewire
      - pipewire-pulse
      - pkg-config
      #- pkg-config-aarch64-linux-gnu
      #- pkg-config-arm-linux-gnueabihf
      #- pkg-config-x86-64-linux-gnu
      - plymouth
      - plymouth-themes
      - policykit-1
      - pristine-tar
      - pristine-lfs
      - psdk
      - pulseaudio-utils
      - python3-debian
      - qemu-user-static
      - qemu-utils
      - rtkit
      - ruby
      - screen
      - splint
      - splint-doc-html
      - strace
      - sudo
      - symlinks
      - sysprof
      - systemd-sysv
      - systemtap
      - systemtap-client
      - systemtap-server
      - tar
      #- tartan
      #- tcmmd
      - u-boot-tools
      - udev
      - usb-modeswitch
      - usbutils
      - valgrind
      - vim-tiny
      - wget
      - whiptail
      - wireless-regdb
      - wireplumber
      - wpasupplicant
      - xauth
      - xdg-desktop-portal
      - xdg-user-dirs
      - xfce4
      - xfce4-screenshooter
      - xfce4-terminal
      - xinput
      - xserver-xephyr
      - xserver-xorg
      - xserver-xorg-input-evdev
      - xserver-xorg-video-fbdev
      - xserver-xorg-video-intel
      - xserver-xorg-video-vesa
      # needed by the VirtualBox extensions for xrandr. (Apertis:T4485)
      - x11-xserver-utils
      - yelp
      - yelp-tools

  - action: apt
    # see APERTIS-6509 and APERTIS-6115 for rationale
    description: "Packages for netboot setup"
    packages:
      - ntp
      - dnsmasq

  - action: run
    description: Set the hostname
    chroot: false
    command: echo "{{ $osname }}" > "$ROOTDIR/etc/hostname"

  - action: overlay
    source: overlays/default-hosts

  - action: overlay
    source: overlays/loopback-interface

  - action: overlay
    source: overlays/iptables-persistence

  - action: overlay
    source: overlays/iptables-rules

  - action: overlay
    source: overlays/media-tmpfs

  - action: overlay
    source: overlays/create-homedir

  - action: overlay
    source: overlays/sudo-fqdn

  - action: overlay
    source: overlays/fsck

  - action: overlay
    source: overlays/sdk-btusb-disable-autosuspend

  - action: overlay
    source: overlays/sdk-vboxvideo-workaround

  - action: overlay
    source: overlays/sdk-lightdm-autologin

  - action: overlay
    source: overlays/sdk-mount-opt-sysroot

  - action: overlay
    source: overlays/sdk-xfce-theme

  - action: overlay
    source: overlays/sdk-desktop-icon-devhelp/

  - action: overlay
    source: overlays/wireplumber

  - action: overlay
    description: "Install ed25519 public keys for Flatpak"
    source: overlays/ed25519-flatpak

  - action: run
    chroot: true
    description: "Enable /tmp mount"
    script: scripts/enable-tmpfs.sh

  - action: run
    chroot: true
    script: scripts/add-xdg-user-metadata.sh

  - action: run
    chroot: true
    script: scripts/create-mtab-symlink.hook.sh

  - action: run
    chroot: true
    script: scripts/setup_user.sh

  - action: run
    description: "Setting up sysroot storage"
    chroot: true
    command: install -d -m 0755 -o user -g user /home/sysroot

  - action: run
    description: "disable deprecated gnome-vfs-2.0 documentation"
    chroot: true
    script: scripts/disable-deprecated-gnome-vfs-2.0.sh

  - action: run
    chroot: true
    script: scripts/add_user_to_groups.sh

  - action: run
    chroot: true
    script: scripts/check_sudoers_for_admin.sh

  - action: run
    chroot: true
    script: scripts/generate_openssh_keys.sh

  - action: run
    chroot: true
    script: scripts/add-initramfs-modules.sh

  - action: run
    chroot: true
    description: "Disable daily apt download, upgrade and clean activities, https://phabricator.apertis.org/T6341"
    command: systemctl disable apt-daily.timer apt-daily-upgrade.timer

  - action: run
    chroot: true
    description: "Disable ntp & dnsmasq services by default"
    command: systemctl disable ntp dnsmasq

  - action: run
    chroot: true
    description: "Disable systemd-timesyncd service by default"
    command: systemctl disable systemd-timesyncd

  - action: run
    chroot: true
    description: "Enable iptables services by default"
    command: systemctl enable iptables

  - action: run
    chroot: true
    description: "Add user to docker group"
    command: usermod -a -G docker user

{{- if eq $pack "true" }}
  - action: run
    description: "Save installed package status"
    chroot: false
    command: gzip -c "${ROOTDIR}/var/lib/dpkg/status" > "${ARTIFACTDIR}/{{ $ospack }}.pkglist.gz"

  - action: run
    description: List files on {{ $ospack }}
    chroot: false
    script: scripts/list-files "$ROOTDIR" | gzip > "${ARTIFACTDIR}/{{ $ospack }}.filelist.gz"

  - action: pack
    compression: gz
    file: {{ $ospack }}.tar.gz
{{- end }}
