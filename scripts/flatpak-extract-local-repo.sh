#!/bin/sh

set -e

ARCH=$1

usage() {
    echo "Usage: $0 ARCH"
    echo "    ARCH      Target architecture"
}

if [ -z "${ARCH}" ]; then
    usage
    exit 1
fi

# The Flatpak installation folder shouldn't be included in the OSTree commit.
# In order to avoid that, we make a tarball from this folder and subsequently
# remove it. The tarball will then be backed up to the artifacts directory so
# is can easily be restored once the OStree commit has been deployed to the
# image
tar czf /flatpak-demo-apps-${ARCH}.tar.gz /var/lib/flatpak
rm -rf /var/lib/flatpak
