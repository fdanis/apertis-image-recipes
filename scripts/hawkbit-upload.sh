#!/bin/sh
set -e

usage() {
	echo "$0 [OPTIONS] user password <command>

	options:
	 -u URL   base URL of HawkBit update server, default to http://localhost:8080
	 -h       display this help and exit

	commands:
	 - target <target id> <name> <security token> <description>
	 - distribution <name> <description> <version> <type> artifacts...
	   type can be 'os', 'app' or 'os_app'
	"
	exit 1
}

check_error() {
	status="$(echo "$1" | jq -r '.status? // empty')"
	case $status in
		""|1*|2*) # succeed if no http code is set in the response, or we have a good one
			return 0
			;;
		*)
			error="$(echo "$1" | jq -r '.error?')"
			message="$(echo "$1" | jq -r '.message?')"
			echo "  ${2-Failed with error}: $status $error\n$message" >&2
			exit 1
			;;
	esac
}

target() {
	echo Adding target...
	resp="$(curl --post301 -L -sS --user $user:$password $url/rest/v1/targets -X POST \
		-H 'Content-Type: application/json;charset=UTF-8' \
		-d "[ { \"controllerId\" : \"$1\", \"name\" : \"$2\", \"securityToken\" : \"$3\", \"description\" : \"$4\"} ]")"
	check_error "$resp"
	echo "Done (target URL: $(echo "$resp" | jq -r '.[]._links.self.href'))"
}

distribution() {
	echo Adding distribution...

	name="$1"
	desc="$2"
	ver="$3"
	type="$4"
	shift 4

	case "$type" in
		os | os_app)
			module_type=os
			;;
		app)
			module_type=application
			;;
		*)
			echo "<type> should be 'os', 'os_app' or 'app'"
			exit 1
			;;
	esac

	echo "  Checking software module..."
	resp="$(curl -L -sS --user $user:$password "$url/rest/v1/softwaremodules?q=name%3D%3DModule$name%3Bversion%3D%3D$ver")"
	check_error "$resp" "Checking software module failed with error"
	if [ "$(echo "$resp" | jq -r '.size')" = 0 ]
	then
		# Create software module
		echo "  Creating software module..."
		resp="$(curl --post301 -L -sS --user $user:$password $url/rest/v1/softwaremodules -X POST \
			-H 'Content-Type: application/json;charset=UTF-8' \
			-d "[ {\"vendor\" : \"Apertis\", \"name\" : \"$name\", \"description\" : \"$desc\", \"type\" : \"$module_type\", \"version\" : \"$ver\" } ]")"
		check_error "$resp" "Creating software module failed with error"
		module_url="$(echo "$resp" | jq -r '.[]._links.self.href')"
		module_id="$(echo "$resp" | jq -r '.[].id')"
	else
		module_url="$(echo "$resp" | jq -r '.content[0]._links.self.href')"
		module_id="$(echo "$resp" | jq -r '.content[0].id')"
	fi

	# Add artifacts to software module
	while [ $# -gt 0 ]
	do
		echo "  Adding artifact: $1"
		echo "URL: $module_url"
		resp="$(curl --post301 -L -v -sS --user $user:$password $module_url/artifacts -X POST \
			-H 'Content-Type: multipart/form-data' -F "file=@$1")"
		error="$(echo "$resp" | jq -r '. | select(.errorCode) | .errorCode')"
		if [ -n "$error" ]
		then
			echo "  Upload artifact failed with error: $error"
			exit 1
		fi
		shift
	done

	echo "  Checking distribution..."
	resp="$(curl -L -sS --user $user:$password "$url/rest/v1/distributionsets?q=name%3D%3D$name%3Bversion%3D%3D$ver")"
	check_error "$resp" "Checking distribution failed with error"
	if [ "$(echo "$resp" | jq -r '.size')" = 1 ]
	then
		os_module="$(echo "$resp" | jq -r '.content[0].modules[] | select(.type == "os") | .id')"
		if [ "$module_id" = "$os_module" ]
		then
			echo "  Distribution set already includes the software module."
			echo "Done (target URL: $(echo "$resp" | jq -r '.content[0]._links.self.href'))"
			return
		fi
		if [ -z "$os_module" ]
		then
			echo "  Error: Distribution exists but doesn't reference the software module $module_id"
			echo "  Cannot proceed."
			exit 0
		fi
	fi

	# Create distribution
	resp=$(curl --post301 -L -sS --user $user:$password $url/rest/v1/distributionsets/ -X POST \
		-H 'Content-Type: application/json;charset=UTF-8' \
		-d "[ {\"requiredMigrationStep\" : false, \"name\" : \"$name\", \"description\" : \"$desc\", \"type\" : \"$type\", \"version\" : \"$ver\", \"modules\" : [ {\"id\" : ${module_id}} ] } ]")
	check_error "$resp"
	echo "Done (target URL: $(echo "$resp" | jq -r '.[]._links.self.href'))"
}

url=http://localhost:8080

while getopts ":hu:" opt
do
	case $opt in
		u)
			url=$OPTARG
			;;
		h)
			usage
			;;
		\? )
			echo Error: Unknown option: -$OPTARG >&2
			echo >&2
			usage
			exit 1
			;;
	esac
done
shift $((OPTIND -1))

if [ $# -lt 2 ]
then
	echo Error: Need the username and the password. >&2
	echo >&2
	usage
	exit 1
fi

user=$1
password=$2
shift 2

case "$1" in
	target)
		shift
		target "$@"
		;;
	distribution)
		shift
		distribution "$@"
		;;
	*)
		usage
		;;
esac
